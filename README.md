# Decorator Pattern #

### What is decorator pattern and how is it useful ? ###
* The concept of a decorator pattern is that it adds additional attributes to an object dynamically.And it will not affect other object or behavior in same class.
* The decorator pattern is often useful for adhering to the [Single Responsibility Principle](http://en.wikipedia.org/wiki/Single_responsibility_principle),as it allows functionality to be divided between classes with unique areas of concern.


![DecoratorPattern.png](https://bitbucket.org/repo/RyE6Ep/images/1738416144-DecoratorPattern.png)


### Example source code and how to apply decorator pattern. ###


```
#!java
/**
* This code in below show example how to use decorator pattern.
* @author Patchara Pattiyathanee 5710546348 
* @version 27 April 2015
*/
```

1)Create some interface for decorator pattern.

```
#!java
public interface Sword {
	public void swordOptionDetail();
}
```

2)Create new class(concrete component) that implements an above interface.

```
#!java
public class SimpleSword implements Sword{

	@Override
	public void swordOptionDetial() {
		System.out.println ("Sword with 300 malee damage");
	}

}
```

3)create abstract class that also implements an above interface (notice that it doesn't implements SwordOptionDetial() )


```
#!java
public abstract class SwordDecorator implements Sword{
		//it better to not encapsulate the attribute
                Sword sword;
		public SwordDecorator(Sword sword) {
			this.sword = sword;
		}
}
```

4)Now we can add more detial to our sword by extends above abstract class,and receive object in constructor

```
#!java

public class FlameSwordDecorator extends SwordDecorator{
	
	public FlameSwordDecorator(Sword sword) {
		super(sword);
	}

	@Override
	public void swordOptionDetial() {
	        sword.swordOptionDetial();
		System.out.println("Sword with burning effect 99dmg per second ");
		darkFlameEffect();
        }
	
	//we can add extra option to our subclass
	public void darkFlameEffect(){
		System.out.println("Additional 5dmg per second burning effect ");
	}
}
```

5)And go on....

```
#!java
public class IceSwordDecorator extends SwordDecorator{
	
	public IceSwordDecorator(Sword sword) {
		super(sword);
	}
	
	@Override
	public void swordOptionDetial() {
	        sword.swordOptionDetial();
		System.out.println( "Sword with freezing effect for 5 seconds ");
		AbsoluteZero();
	}
	
	public void AbsoluteZero(){
		System.out.println("After ice shatter,can cause slow effect for 10 seconds");
	}
	
	//.......

}
```
6) Main class 

```
#!java
public class Main {
	public static void main(String[] args) {
		Sword excalibur = new FlameSwordDecorator(new IceSwordDecorator(new SimpleSword())); //it can be very long
		excalibur.swordOptionDetial();
	}
                //result from my code
		/*      Sword with 300 malee damage
			Sword with freezing effect for 5 seconds 
			After ice shatter,can cause slow effect for 10 seconds
			Sword with burning effect 99dmg per second 
			Additional 5dmg per second burning effect  */
}

```

### Exercise ###
Apply decorator pattern to design ice-cream with many toppings shop.
Each toppings have different price,so if customer add toppings to their ice-cream
until they satisfy program should calculate the summary price.